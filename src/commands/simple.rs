extern crate rand;

use rand::prelude::*;

command!(rollthedice(_ctx, msg) {
    let mut rng = thread_rng();
    let n: i32 = rng.gen_range(0, 100);
    let _ = msg.channel_id.say(format!("You rolled (0 - 100): {}", n));
});

command!(flipcoin(_ctx, msg) {
    let mut rng = thread_rng();
    let n: bool = rng.gen_bool(0.5);
    let answer;
    if n {
        answer = format!("Heads!");
    }
    else {
        answer = format!("Tails");
    }
    let _ = msg.channel_id.say(format!("You flipped a coin: {}", answer));
});