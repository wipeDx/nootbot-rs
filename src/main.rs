#[macro_use] extern crate serenity;
#[macro_use] extern crate log;

extern crate env_logger;
extern crate kankyo;
extern crate rand;

mod commands;

use serenity::framework::standard::{StandardFramework, help_commands};
use serenity::prelude::*;
use serenity::model::event::ResumedEvent;
use serenity::model::gateway::Ready;
use serenity::http;

use std::collections::HashSet;
use std::env;

struct Handler;

impl EventHandler for Handler {
    fn resume (&self, _: Context, _: ResumedEvent) {
        info!("Resumed");
    }

    // Handler when shard is booted
    fn ready(&self, _ctx: Context, ready: Ready) {
        info!("{} connected!", ready.user.name);
        _ctx.set_game_name("Need help? .help");
    }
}

fn main() {
    // Load env variables
    kankyo::load().expect("Failed to load .env file");

    // Initialize logger
    env_logger::init().expect("Failed to initialize env_logger");

    let token = env::var("DISCORD_TOKEN")
        .expect("Expected a token in the environment");
    
    let mut client = Client::new(&token, Handler).expect("Error creating the client.");

    // set owners
    let owners = match http::get_current_application_info() {
        Ok(info) => {
            let mut set = HashSet::new();
            set.insert(info.owner.id);

            set
        },
        Err(e) => panic!("Couldn't get application info: {}", e),
    };


    // Setup clients
    client.with_framework(StandardFramework::new()
        .configure(|c| c
            .owners(owners)
            .prefix(".")
            .allow_whitespace(true))
        .group("General", |g| {
            g.command("Noot", |c| c.cmd(commands::noot::noot)
                .desc("The whole reason this bot exists. Try it!")
                .batch_known_as(["noot"].iter()))
             .command("Buff", |c| c.cmd(commands::noot::buff)
                .desc("The second command ever created, you should also try it!")
                .batch_known_as(["buff"].iter()))
        })
        .group("RNGs", |g| {
            g.command("RollTheDice", |c| c.cmd(commands::simple::rollthedice)
                .desc("Rolls a dice between two boundaries")
                .min_args(0).max_args(2)
                .batch_known_as(["roll", "dice", "rtd", "rollthedice"].iter()))
            .command("Flip", |c| c.cmd(commands::simple::flipcoin)
                .desc("Flips a coin!")
                .num_args(0)
                .batch_known_as(["flip", "flop", "flipflop", "flopflip"].iter()))
        })
        .group("Admin Commands", |g| {
            g.command("Game", |c| c.cmd(commands::admin::changegame)
                .desc("Changes the game that the Bot is \"Playing\"")
                .min_args(1)
                .batch_known_as(["game", "changegame", "gamename"].iter()))
        }
        .owners_only(true)
        .guild_only(true))
        
        .help(help_commands::with_embeds)
        );
    
    // Start single shard
    if let Err(why) = client.start() {
        println!("Client error: {}", why);
    }
}
